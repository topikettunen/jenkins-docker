#!/bin/bash

docker stack rm slaves
docker stack rm selenium-grid
docker stack rm quarantine
docker stack rm mysql
docker stack rm grafana-influx
docker stack rm jenkins