#!/bin/bash

docker stack deploy -c jenkins-master/jenkins.yml jenkins
docker stack deploy -c jenkins-slaves/slave.yml slaves
docker stack deploy -c selenium-grid/grid.yml selenium-grid
docker stack deploy -c quarantine-rest/quarantine.yml quarantine
docker stack deploy -c mysql/mysql.yml mysql
docker stack deploy -c grafana-influx/grafana-influx.yml grafana-influx