#!/bin/sh

# curl -i -XPOST "http://localhost:8086/query" --data-urlencode "q=create database telegraf"

curl -i -XPOST "http://admin:admin@localhost:3001/api/datasources" -H 'Content-Type: application/json;charset=UTF-8' --data-binary '{"Name":"Telegraf","type":"influxdb","url":"http://influx:8086","access":"proxy","isDefault":false,"database":"telegraf","user":"robot","password":"robot"}'
