#!/bin/sh

curl -i -XPOST "http://localhost:8086/query" --data-urlencode "q=create database robotframework"

curl -i -XPOST "http://localhost:8086/query" --data-urlencode "q=create user robot with password 'robot' with all privileges"

curl -i -XPOST "http://admin:admin@localhost:3001/api/datasources" -H 'Content-Type: application/json;charset=UTF-8' --data-binary '{"Name":"Robot Framework","type":"influxdb","url":"http://influx:8086","access":"proxy","isDefault":true,"database":"robotframework","user":"robot","password":"robot"}'
