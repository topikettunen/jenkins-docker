#!/bin/bash

STACK_NAME="grafana-influx"
TR_CONTAINER_NAME="telegraf"

TR_CONTAINER_ID=$(docker ps --filter name="$STACK_NAME"_"$TR_CONTAINER_NAME" -q)

docker exec "$TR_CONTAINER_ID" telegraf -sample-config -input-filter statsd -output-filter influxdb > telegraf.conf
